# FirstRust

Requirements:

cargo, npm, and python3 must be installed.

To build and launch the FirstRust app locally:

1. ENV=prod npm run pkg
2. cd pkg
3. ./run.py

Use a competent browser to navigate to the ip:port address indicated.
By default, a tab-less Chrome window will attempt to launch.
The page will prompt for n, in order to benchmark fib(n) in JavaScript and Rust+Wasm.

WARNING: n >= 45 will take several minutes to calculate.
